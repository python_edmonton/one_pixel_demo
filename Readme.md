# One pixel attack
This repo implements a version of the One pixel attack paper published by Jiawei Su, Danilo Vasconcellos Vargas and Sakurai Kouichi. Ref. https://arxiv.org/pdf/1710.08864.pdf 
The current version only supports attacks with a single pixel. A notebook with examples is available for a quick look at the results. The demo is infering using a pretrained VGG16 network.
As a lot of inferences are performed during the attack, a GPU is advised to run the notebook (order of 10^4 inferences for 100 candidates over 100 generations)

### Requirements

- pytorch
- torchvision
- matplotlib
- numpy
- seaborn
- jupyter

Note : to install pytorch and torch vision : 

> pip3 install torch torchvision


### Run notebook

> jupyter notebook 

Open one_pixel_attack.ipynb

### Presentation

A few slides trying to provide an introduction to the concepts related to this demo is available here : 

https://docs.google.com/presentation/d/1fMZukYn71zCg7Wp41Jhfey1_O-zhHW4NXP8eO-a12gQ/edit?usp=sharing

### Library for plotting gradient
The following library has been used to see how the one pixel attacks modifies the activation of the neural network.

https://github.com/utkuozbulak/pytorch-cnn-visualizations 
