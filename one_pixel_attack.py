import torch
import torchvision.models as models
from package import DE, vgg_tools
import numpy as np
import random
if __name__ == '__main__':

    manualSeed = 1

    np.random.seed(manualSeed)
    random.seed(manualSeed)
    torch.manual_seed(manualSeed)
    # if you are suing GPU
    torch.cuda.manual_seed(manualSeed)
    torch.cuda.manual_seed_all(manualSeed)

    torch.backends.cudnn.enabled = False
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True    #Image files and class definition
    image_dict = {}
    # image_dict["data/cat.jpg"] = "cat"
    # image_dict["data/cat_1.jpg"] = "cat"
    # image_dict["data/cat_2.jpg"] = "cat"
    # image_dict["data/ball.jpg"] = "ball"
    # image_dict["data/jackOlantern.jpg"] = "jack,lantern"
    # image_dict["data/pizza.jpg"] = "pizza"
    # image_dict["data/traffic_light.jpg"] = "traffic,light"
    # image_dict["data/indian_cobra.jpg"] = "cobra,snake"
    # image_dict["data/firetruck.jpg"] = "fire,truck"
    image_dict["data/cat.jpg"] = "cat"
    image_dict["data/elephant.jpg"] = "elephant"
    image_dict["data/panda.jpg"] = "panda"

    torch.set_default_tensor_type('torch.FloatTensor')
    vgg = models.vgg16(pretrained=True).cuda()

    for key in image_dict:
        input_tensor = vgg_tools.image_to_torch_tensor(key)

        reference_results = vgg_tools.extract_top_n_from_output(vgg_tools.generate_model_output(input_tensor, vgg), 5)
        reference_image = vgg_tools.tensor_to_image(input_tensor)


        print("Image : {}, reference output : {}".format(key, reference_results))
    for key in image_dict:
        input_tensor = vgg_tools.image_to_torch_tensor(key)
        reference_results = vgg_tools.extract_top_n_from_output(vgg_tools.generate_model_output(input_tensor, vgg), 5)
        reference_image = vgg_tools.tensor_to_image(input_tensor)
        print("Pixel Attack - Image : {}".format(key))
        differential_evolution = DE.differential_evolution(image_name=key, candidate_nb=100,
                                                           image_size_x=224,
                                                           image_size_y=224,
                                                           generation_nb=100,
                                                           reference_image_as_tensor=input_tensor,
                                                           cuda_model=vgg,
                                                           class_keyword=image_dict[key])
        differential_evolution.set_initial_NN_results(reference_results)
        differential_evolution.generate_first_candidate_generation()
        differential_evolution.run()