import torch
import torchvision
import numpy as np
import matplotlib.image as mpimg
from skimage.transform import resize


def image_to_torch_tensor(image_filename):
    img = mpimg.imread(image_filename)
    img = resize(img, (224, 224), anti_aliasing=True)
    img = np.transpose(img, (2, 1, 0))
    img = torch.from_numpy(img)
    img = torchvision.transforms.Normalize(mean=[0.485,0.456,0.406],
                                            std=[0.229,0.224,0.225])(img)
    img.unsqueeze_(0)
    img = img.float().cuda()
    return img


def tensor_to_image(tensor):
    tensor = tensor.cpu()
    tensor = tensor.numpy()
    tensor = np.squeeze(tensor, axis=0)
    tensor = np.transpose(tensor, (2, 1, 0))
    return tensor


def generate_key_to_classname_list():
    with open('data/imagenet_synsets.txt', 'r') as f:
        synsets = f.readlines()

        synsets = [x.strip() for x in synsets]
        splits = [line.split(' ') for line in synsets]
        key_to_classname = {spl[0]: ' '.join(spl[1:]) for spl in splits}
    return key_to_classname


def generate_class_id_to_key():
    with open('data/imagenet_classes.txt', 'r') as f:
        class_id_to_key = f.readlines()
        class_id_to_key = [x.strip() for x in class_id_to_key]
    return class_id_to_key


def generate_model_output(img, model):
    output = model(img)
    min_value = torch.min(output, 1)
    output = torch.add(output, - min_value[0])
    m = torch.nn.Softmax(dim=1)
    output = m(output.data)
    return output


def extract_top_n_from_output(output, n):
    key_to_classname = generate_key_to_classname_list()
    class_id_to_key = generate_class_id_to_key()
    ntop = []
    top5, indextop5 = output.topk(n)
    for top, indextop in zip(top5.data.squeeze(), indextop5.data.squeeze()):
        for index, key in enumerate(class_id_to_key):
            if indextop == index:
                ntop.append([key_to_classname[key], str(float(top))])
    return ntop
