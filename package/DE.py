import copy
import os
import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from package.vgg_tools import tensor_to_image, generate_model_output, extract_top_n_from_output
import seaborn as sns
import numpy as np
plt.interactive(False)


def change_pixel_of_image(pixel, image_as_tensor):
    image = copy.deepcopy(image_as_tensor)
    image[0][0][pixel.x][pixel.y] = pixel.r
    image[0][1][pixel.x][pixel.y] = pixel.g
    image[0][2][pixel.x][pixel.y] = pixel.b
    return image


def plot_image_with_pixel_modified(input_tensor, pixel_candidate, image_name, generation, save_image=False):
    plt.clf()
    # plt.tick_params(
    #     axis='x',  # changes apply to the x-axis
    #     which='both',  # both major and minor ticks are affected
    #     bottom=False,  # ticks along the bottom edge are off
    #     top=False,  # ticks along the top edge are off
    #     labelbottom=False)
    # plt.tick_params(
    #     axis='y',  # changes apply to the x-axis
    #     which='both',  # both major and minor ticks are affected
    #     bottom=False,  # ticks along the bottom edge are off
    #     top=False,  # ticks along the top edge are off
    #     labelbottom=False)
    image = tensor_to_image(input_tensor)
    image *= (1 / image.max())
    fig, ax = plt.subplots(1, figsize=(8, 8))
    circle_annotation = patches.Circle((pixel_candidate.pixel[0].x, pixel_candidate.pixel[0].y), 5, fill=False,
                                       color='b')
    ax.imshow(image.astype(np.float))
    ax.add_patch(circle_annotation)
    if save_image:
        plt.savefig(
            image_name[0].split(".")[0] + "/" + str(generation) + "_" + str(pixel_candidate.result[0][0]).replace(" ",
                                                                                                                  "_").replace(
                ",", "") + "_" + str(pixel_candidate.result[0][1]) + "_" + str(pixel_candidate.score) + ".jpg")
    else:
        plt.plot()
        plt.show()
    plt.close()


def plot_image(input_tensor, image_name, save_image=False):
    plt.clf()
    # plt.tick_params(
    #     axis='x',  # changes apply to the x-axis
    #     which='both',  # both major and minor ticks are affected
    #     bottom=False,  # ticks along the bottom edge are off
    #     top=False,  # ticks along the top edge are off
    #     labelbottom=False)
    # plt.tick_params(
    #     axis='y',  # changes apply to the x-axis
    #     which='both',  # both major and minor ticks are affected
    #     bottom=False,  # ticks along the bottom edge are off
    #     top=False,  # ticks along the top edge are off
    #     labelbottom=False)
    image = tensor_to_image(input_tensor)
    image *= (1 / image.max())
    fig, ax = plt.subplots(1, figsize=(8, 8))

    ax.imshow(image.astype(np.float))
    if save_image:
        plt.savefig(image_name[0].split(".")[0] + "/reference_+" + image_name[0].split(".")[0] + ".jpg")
    else:
        plt.plot()
        plt.show()
    plt.close()


class pixel_candidate():

    def __init__(self, pixel, generation):
        self.score = 0
        self.pixel = pixel,
        self.generation = generation
        self.result = []

    def get_score(self, reference_output, image_keywords):
        # comparaison des classes
        has_changed_class = True
        for keyword in image_keywords:
            if keyword in self.result[0][0]:
                has_changed_class = False
                break
        if has_changed_class:
            self.score = (1 + abs(float(self.result[0][1])) + abs(float(reference_output[0][1])))
        else:
            self.score = (float(reference_output[0][1]) - float(self.result[0][1]))


class pixel():
    def __init__(self, r, g, b, x, y, image_size_x, image_size_y):
        self.r = r
        self.g = g
        self.b = b
        self.x = min(image_size_x - 1, int(x))
        self.y = min(image_size_y - 1, int(y))
        self.score = 0


class differential_evolution():

    def __init__(self, image_name, candidate_nb, image_size_x, image_size_y, generation_nb, reference_image_as_tensor,
                 cuda_model, class_keyword):
        self.image_name = image_name,
        self.candidate_nb = candidate_nb
        self.image_size_x = image_size_x
        self.image_size_y = image_size_y
        self.generation_nb = generation_nb
        self.reference_image_as_tensor = reference_image_as_tensor
        self.candidates = []
        self.candidates_history = []
        self.cuda_model = cuda_model
        self.class_keyword = class_keyword.split(",")
        self.attack_successful = False

    def run(self):
        plot_image(self.reference_image_as_tensor, self.image_name)
        self.create_image_folder()
        self.generate_first_candidate_generation()

        for generation in range(self.generation_nb):
            print("DE - Génération {}".format(generation))
            self.evaluate_generation()
            best_candidate = self.find_best_candidate()
            modified_tensor = change_pixel_of_image(best_candidate.pixel[0], self.reference_image_as_tensor)
            plot_image_with_pixel_modified(modified_tensor, best_candidate, self.image_name, generation)
            self.generate_next_generation()
            self.plot_candidates_as_kde(generation)
            if (self.attack_successful):
                break
        return self.candidates

    def create_image_folder(self):
        try:
            dirname = os.path.dirname(__file__)
            output_dir = os.path.join(dirname, '../' + self.image_name[0].split(".")[0])
            os.mkdir(output_dir)
        except:
            pass

    def find_best_candidate(self):
        best_candidate = None
        best_score = 0
        attacked_count = 0
        for candidate in self.candidates:
            if candidate.score > best_score:
                best_candidate = candidate
                best_score = candidate.score
            if candidate.score > 1:
                attacked_count += 1
                self.attack_successful = True
        print(
            "Best candidate - classe : {0:s}, confidence : {1:.2f}, score : {2:.2f}, r : {3:.2f}, g : {4:.2f}, b : {5:.2f}, x : {6:d}, y : {7:d}".format(
                best_candidate.result[0][0], float(best_candidate.result[0][1]), float(best_candidate.score),
                float(best_candidate.pixel[0].r), float(best_candidate.pixel[0].g), float(best_candidate.pixel[0].b),
                int(best_candidate.pixel[0].x), int(best_candidate.pixel[0].y)))
        return best_candidate

    def set_initial_NN_results(self, reference_results):
        self.reference_result = reference_results

    def generate_first_candidate_generation(self):
        self.candidates = [pixel_candidate(pixel(random.random() * float(self.reference_image_as_tensor.max()),
                                                 random.random() * float(self.reference_image_as_tensor.max()),
                                                 random.random() * float(self.reference_image_as_tensor.max()),
                                                 random.randint(0, self.image_size_x - 1),
                                                 random.randint(0, self.image_size_y - 1), self.image_size_x,
                                                 self.image_size_y), 0) for i in range(0, self.candidate_nb)]

    def evaluate_generation(self):
        for candidate in self.candidates:
            self.evaluate_candidate(candidate)

    def evaluate_candidate(self, candidate):
        if candidate.score == 0:
            mod_tensor = change_pixel_of_image(candidate.pixel[0], self.reference_image_as_tensor)
            result = extract_top_n_from_output(generate_model_output(mod_tensor, self.cuda_model), 5)
            candidate.result = result
            candidate.get_score(self.reference_result, self.class_keyword)

    def generate_next_generation(self):
        if len(self.candidates_history) == 0:
            self.candidates_history.append(self.candidates)
        else:
            new_candidates = []
            for index, candidate in enumerate(self.candidates):
                candidate_children = self.generate_candidate_child(candidate)
                self.evaluate_candidate(candidate_children)
                if candidate.score > candidate_children.score:
                    new_candidates.append(candidate)
                else:
                    new_candidates.append(candidate_children)
            self.candidates_history.append(new_candidates)
            self.candidates = new_candidates


    def plot_candidates_as_kde(self, generation, save_file=False):
        plt.clf()
        x_coords = []
        y_coords = []
        color = []
        f, (ax1, ax2) = plt.subplots(1, 2, sharey=True, sharex=True, figsize=(16,8))
        ax1.set_aspect("equal")
        ax2.set_aspect("equal")
        for candidate in self.candidates:
            x_coords.append(candidate.pixel[0].x)
            y_coords.append(candidate.pixel[0].y)
            color.append(candidate.score)


        sns.kdeplot(x_coords, y_coords, cmap="Blues", shade=True, shade_lowest=False, ax=ax1).set_title('Candidates KDE')
        blue = sns.color_palette("Blues")[-2]

        sns.scatterplot(x=x_coords, y=y_coords,hue=color, size=color,ax=ax2).set_title('Candidates localisation (score as weight)')
        if save_file:
            plt.savefig(self.image_name[0].split(".")[0] + "/candidates_KDE_" + str(generation) + ".jpg")
        else:
            plt.show()
        plt.close()

    def generate_candidate_child(self, parent_candidate):
        parent_candidates = [random.randint(0, len(self.candidates) - 1) for i in range(3)]
        try:
            child_candidate = pixel_candidate(
                pixel((self.candidates[parent_candidates[0]].pixel[0].r + 0.5 * (
                        self.candidates[parent_candidates[1]].pixel[0].r +
                        self.candidates[parent_candidates[2]].pixel[0].r)) / 2,
                      (self.candidates[parent_candidates[0]].pixel[0].g + 0.5 * (
                              self.candidates[parent_candidates[1]].pixel[0].g +
                              self.candidates[parent_candidates[2]].pixel[0].g)) / 2,
                      (self.candidates[parent_candidates[0]].pixel[0].b + 0.5 * (
                              self.candidates[parent_candidates[1]].pixel[0].b +
                              self.candidates[parent_candidates[2]].pixel[0].b)) / 2,
                      (self.candidates[parent_candidates[0]].pixel[0].x + 0.5 * (
                              self.candidates[parent_candidates[1]].pixel[0].x +
                              self.candidates[parent_candidates[2]].pixel[0].x)) / 2,
                      (self.candidates[parent_candidates[0]].pixel[0].y + 0.5 * (
                              self.candidates[parent_candidates[1]].pixel[0].y +
                              self.candidates[parent_candidates[2]].pixel[0].y)) / 2,
                      self.image_size_y,
                      self.image_size_x),
                parent_candidate.generation + 1,
            )
            return child_candidate

        except Exception as e:
            print(e)
            print(parent_candidates)
            return parent_candidate
